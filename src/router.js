import Home from './component/Home.vue'
import Stock from './component/stocks/Stock.vue'
import StockManagement from './component/stocks/StockManagement.vue'
import Portfolio from './component/portfolio/Portfolio.vue'

export const routes = [
	{path: "", component: Home},
	{path: "/stock", component: Stock},
	{path: "/stock-management", component: StockManagement},
	{path: "/portfolio", component: Portfolio}
]