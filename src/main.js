import Vue from 'vue'
import App from './App.vue'
import VueRounter from 'vue-router'
import VueResource from 'vue-resource'
import {routes} from './router'
import {store} from './store'

Vue.use(VueRounter);
Vue.use(VueResource);

Vue.http.options.root = "https://ori-stock-trading.firebaseio.com/";

Vue.filter('currency', function(value){
	return "$" + value;
});

const router = new VueRounter({
	routes,
	mode: "history"
})

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
