import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
export const store = new Vuex.Store({
	state: {
		funds: 0,
		selectedStock: {
			name: "",
			price: 0,
			quantity: 0
		}
	}
})